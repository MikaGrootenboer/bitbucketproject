Deployhq repolink changes
=
this repo has the code to change the repo url.
before that there must be a curl request to get the current names and repo's


`curl -H "Content-type: application/json" \
-H "Accept: application/json" \
--user serverbeheer@redkiwi.com:my-api-key \
https://test.deployhq.com/projects/ >> filename.txt
`

this file needs to be added in `deploy_json.py`


the final step is to run this file in terminal with.

`python3.7 deploy_json.py`


Autorep
=
In this repo you will find a second `.py` file named `autorep.py`
with this file you will be able to change the repo url to the new
one.

Follow the next steps to correctly rename the git url:
    
- Copy the `autorep.py` to the folder you want to update
- open your Terminal and cd to the map
- run `python3.7 autorep.py` (or another version of python)
- Delete the `autorep.py` once you're done :)
    