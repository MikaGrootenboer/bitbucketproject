import os

# options : git@bitbucket.org:redkiwi-wp/ , git@bitbucket.org:redkiwi-general/ , git@bitbucket.org:redkiwi/ , git@bitbucket.org:redkiwi-sites/ ,
old = os.popen("git remote get-url --push origin").read()
new = "https://redkiwi.nl"
def just_change(newurl,oldlength):
    # getting the current repository url
    os.system("echo " + "==========Getting old remote url==========")
    current = os.popen("git remote get-url --push origin").read()
    print("the old url ..." +current)
    # removing 'git@bitbucket.org' from the repo
    current_final = current[oldlength:]
    print("the ending that will be kept..."+current_final)

    # new url
    # placing the new url infront of the location
    command = f'''git remote set-url origin {newurl}{current_final}'''
    os.system(command)
    os.system("echo " + "==========Creating new url==========")
    os.system("echo " +"the new url..."+ newurl + current_final)



if (old[:29]=="git@bitbucket.org:redkiwi-wp/"):
    print("wordpress")
    just_change(new, 29)
elif(old[:34]=="git@bitbucket.org:redkiwi-general/"):
    print("general")
    just_change(new, 34)
elif(old[:32]=="git@bitbucket.org:redkiwi-sites/"):
    print("sites")
    just_change(new, 32)
elif(old[:26]=="git@bitbucket.org:redkiwi/"):
    print("redkiwi")
    just_change(new, 26)
elif(old[:34]=="git@bitbucket.org:MikaGrootenboer/"):
    print("MIKAAA")
    just_change(new,34)
else:
    print("sorry your url won't work with this programm, slack mika for details")

